# Crosswords Puzzle Sets for Oedipus

This repo contains an Italian puzzle set downloader for [GNOME Crosswords](https://gitlab.gnome.org/jrb/crosswords). The puzzles are pulled from [Oedipus](http://web.tiscali.it/oedipus).
